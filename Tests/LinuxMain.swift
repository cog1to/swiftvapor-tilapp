import XCTest
@testable import AppTests

XCTMain([
  testCase(AcronymTests.allTests),
  testCase(UserTests.allTests)
])
